// Load the expressjs module into our appliction and saved it in a variable called express.
// syntax: require ("package");
const express = require("express");

// localhost port number
const port = 4000;

// app is our server
//create an application that uses express and stores it as app
const app = express();

// middleware
// express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming JSON from our req.body
app.use(express.json());

// mock data
let users =[
	{
		username: "TStark3000",
		email: "starksInsudtries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	}

];

let items = [
	{
		name: "Mjolnir",
		price: 50000,
		isActive: true
	},
	{
		name: "Vibranium Shield",
		price: 70000,
		isActive: true
	}
];

// Express has method to use as routes corresponding to HTTP methods.
// Syntax: app.method(<endpoint>,<function for req and res>)

app.get('/', (req, res) => {

	// res.send() actually combines writeHead() and end()
	res.send(`Hello from my first expressJS API`);
	// res.status(200).send("message");
});

// SOLUTION

app.get("/greeting", (req, res) => {
	res.send(`Hello from Batch203-Orongan`);
});

//retrieval of the users in mock database
app.get('/users', (req, res) => {

	// res.send already stringifies for you
	res.send(users);
});

// adding of new user
app.post('/users', (req, res) => {

	console.log(req.body); // result: {} empty object

	// simulate creating a new user document
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	};

	// adding newUser to the existing array
	users.push(newUser);
	console.log(users);

	// send the updated user array to the client
	res.send(users);

});

// DELETE Method
app.delete('/users', (req, res) => {

	users.pop();
	res.send(users);
});

// PUT Method
// update user's password
// :index - wildcard
// url: localhost:4000/users/0

app.put('/users/:index', (req, res) => {

	console.log(req.body); // updated password
	// result: {}

	// an object that contains the value of the URL params
	console.log(req.params); // refer to the wildcard in the endpoint
	// results: {index: 0}


	//parseInt the value of the number coming from req.paramas
	//['0'] turns into [0]
	let index = parseInt(req.params.index);

	//users[0].password
		//"notPeterParker"= "TonyStark01"
		// the update will be coming from the request body
	users[index].password = req.body.password;

	// send the updated user
	res.send(users[index]);

});

/*
Activity : 30 mins

    Endpoint: /items

    >> Create a new route to get and send items array in the client (GET ALL ITEMS)

    >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
        >> send the updated items array in the client
        >> check the post method route for our users for reference

    >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
        >> pass the index number of the item that you want to update in the request params
        >> add the price update in the request body
        >> reassign the new price from our request body
        >> send the updated item in the client

    >> Create a new collection in Postman called s34-activity
    >> Save the Postman collection in your s34 folder

 */

app.get('/items', (req, res) => {
	res.send(items);
});

app.post('/items', (req, res) => {

	console.log(req.body); 
	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	};
	items.push(newItem);
	console.log(items);

	res.send(items);

});

app.put('/items/:index', (req, res) => {

	console.log(req.body); 
	console.log(req.params); 

	let index = parseInt(req.params.index);
	items[index].price = req.body.price;

	res.send(items[index]);

});



// port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));






